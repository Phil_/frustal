/*
 * This module aims to provide the user with an
 * easy way to render parts of the mandelbrot set
 * in high res as single images or animations
 *
 * TODO: provide an easy way to benchmark the calculation by rendering the same
 * frame/animation over and over then average the timings
 * TODO: plot log.csv in rust (?) (see plotters)
 */

extern crate image;
extern crate clap;
extern crate rayon;

use std::process::Command;
use std::time;
use std::fs::{OpenOptions};
use std::io::{stdout, Write};
use rayon::prelude::*;
use clap::{App, Arg, SubCommand, AppSettings};

fn main() {
    let matches = App::new("frustal")
        .version("1.0")
        .setting(AppSettings::AllowNegativeNumbers)
        .author("Philipp Herzog et al")
        .about("Calculates fractals in rust")
        .arg(Arg::with_name("width")
             .short("w")
             .long("width")
             .default_value("256")
             .value_name("width")
             .help("image width in pixels")
             .display_order(1))
        .arg(Arg::with_name("height")
             .short("h")
             .long("height")
             .default_value("256")
             .value_name("height")
             .help("image height in pixels")
             .display_order(2))
        .arg(Arg::with_name("coord")
             .short("x")
             .takes_value(true)
             .number_of_values(4)
             .value_names(&["x1", "x2", "y1", "y2"])
             .help("Coordinates of the initial area")
             .display_order(3))
        .arg(Arg::with_name("maxiter")
             .short("i")
             .long("maxiter")
             .default_value("256")
             .value_name("maxiter")
             .help("max number of iterations per pixel")
             .display_order(4))
        .arg(Arg::with_name("type")
             .short("t")
             .long("type")
             .default_value("0")
             .value_name("type")
             .help("0=stride, 1=stripe, 2=blockmaster")
             .display_order(5))
        .arg(Arg::with_name("outfile")
             .short("o")
             .long("outfile")
             .default_value("image.pgm")
             .value_name("outfile")
             .help("outfile to write to")
             .display_order(6))
        .arg(Arg::with_name("verbose")
             .short("v")
             .help("verbosity level, multiple 'v's for increased verbosity (max 2)")
             .multiple(true)
             .display_order(7))
        .arg(Arg::with_name("logging")
             .short("l")
             .value_name("file")
             .default_value("log.csv")
             .help("enables logging")
             .display_order(8))
        .arg(Arg::with_name("numprocs")
             .value_name("numprocs")
             .default_value("4")
             .help("number of processe")
             .display_order(9))
        .arg(Arg::with_name("nosave")
             .long("nosave")
             .multiple(false)
             .help("prevents saving the image\nImplicitly sets logging to true")
             .display_order(10))
        .subcommand(SubCommand::with_name("animation")
                    .setting(AppSettings::AllowNegativeNumbers)
                    .about("Animation rendering")
                    .version("1.0")
                    .author("Philipp Herzog et al")
                    .arg(Arg::with_name("width")
                         .short("w")
                         .default_value("256")
                         .value_name("width")
                         .help("animation width in pixels")
                         .display_order(1))
                    .arg(Arg::with_name("maxiter")
                         .short("i")
                         .long("maxiter")
                         .default_value("256")
                         .value_name("maxiter")
                         .help("max number of iterations per pixel")
                         .display_order(4))
                    .arg(Arg::with_name("height")
                         .short("h")
                         .default_value("256")
                         .value_name("height")
                         .help("animation height in pixels")
                         .display_order(2))
                    .arg(Arg::with_name("coord_start")
                         .long("xstart")
                         .takes_value(true)
                         .number_of_values(4)
                         .value_names(&["x1", "x2", "y1", "y2"])
                         .help("Coordinates of the initial area")
                         .display_order(3))
                    .arg(Arg::with_name("coord_end")
                         .long("xend")
                         .takes_value(true)
                         .value_names(&["x1", "x2", "y1", "y2"])
                         .number_of_values(4)
                         .help("Coordinates of the end area")
                         .display_order(4))
                    .arg(Arg::with_name("fps")
                         .long("fps")
                         .default_value("25")
                         .value_name("fps")
                         .help("frames per second")
                         .display_order(5))
                    .arg(Arg::with_name("duration")
                         .long("duration")
                         .default_value("5")
                         .value_name("duration")
                         .help("Animation duration in seconds")
                         .display_order(6))
                    .arg(Arg::with_name("outfile")
                         .short("o")
                         .long("outfile")
                         .default_value("out.mp4")
                         .value_name("file")
                         .help("outfile to write to")
                         .display_order(7))
                    .arg(Arg::with_name("logging")
                         .short("l")
                         .default_value("log.csv")
                         .value_name("file")
                         .help("enables logging")
                         .display_order(8))
                    .arg(Arg::with_name("type")
                         .short("t")
                         .long("type")
                         .default_value("0")
                         .value_name("type")
                         .help("0=stride, 1=stripe, 2=blockmaster")
                         .display_order(9))
                    .arg(Arg::with_name("verbose")
                         .short("v")
                         .help("verbosity level, multiple 'v's for increased verbosity (max 2)")
                         .multiple(true)
                         .display_order(10)))
                         .get_matches();

    // clear log
    let conf = Config::new(&matches);
    if let Logging::True(logfile) = &conf.logging {
        if std::path::Path::new(&logfile.clone()).exists() {
            std::fs::remove_file(logfile).unwrap();
        }
    }

    let mut total_calc = 0.;
    let mut total_flat = 0.;
    let mut total_save = 0.;

    if conf.verbose_level > 0 { conf.print(); }

    let start_all = time::Instant::now();
    for (count, frame) in conf.iter().enumerate() {
        let start_frame = time::Instant::now();
        // render the image
        let mut start_section = time::Instant::now();
        let res_img = calc(frame, conf.width, conf.height, conf.maxiter);
        let calc_dur = start_section.elapsed().as_millis() as f64 / 1000.0;
        total_calc += calc_dur;

        let flat_dur;
        let save_dur;

        if ! conf.nosave {
            // flatten to one-d buffer + scale to u8
            start_section = time::Instant::now();
            let onedarr = res_img.into_iter().flatten().collect::<Vec<u32>>();
            let max = *onedarr.par_iter().max().unwrap();
            let onedarr_scaled: Vec<u8>;
            if max > 255 {
                onedarr_scaled = onedarr.into_par_iter().map(|x| {(255.0 * (x as f64 / max as f64)) as u8}).collect();
            } else {
                onedarr_scaled = onedarr.into_par_iter().map(|x| { x as u8 }).collect();
            }
            flat_dur = start_section.elapsed().as_millis() as f64 / 1000.0;
            total_flat += flat_dur;

            // then save to disk
            start_section = time::Instant::now();
            save_as_png(&onedarr_scaled, &conf, count);
            save_dur = start_section.elapsed().as_millis() as f64 / 1000.0;
            total_save += save_dur;
        } else {
            flat_dur = 0.;
            save_dur = 0.;
        }

        let frame_dur = start_frame.elapsed().as_millis() as f64 / 1000.0;

        // ################################
        // logging, each frame
        if let Logging::True(filename) = &conf.logging {
            // TODO maybe move this somewhere else for performance


            if let RenderType::Animated(_) = &conf.render_type {
                let mut file;
                if std::path::Path::new(&filename).exists() {
                    file = OpenOptions::new().write(true).append(true).open(filename).unwrap();
                } else {
                    file = OpenOptions::new().write(true).create(true).open(filename).unwrap();
                    writeln!(file, "{}", "frame,time,type").unwrap();
                }
                writeln!(file, "{}, {}, calc",  count, calc_dur).unwrap();
                if ! conf.nosave {
                    writeln!(file, "{}, {}, flat",  count, flat_dur).unwrap();
                    writeln!(file, "{}, {}, save",  count, save_dur).unwrap();
                    writeln!(file, "{}, {}, total", count, frame_dur).unwrap();
                }
            } else {
                let mut file = stdout();
                writeln!(file, "{}, {}, calc",  conf.numprocs, calc_dur).unwrap();
                if ! conf.nosave {
                    writeln!(file, "{}, {}, flat",  conf.numprocs, flat_dur).unwrap();
                    writeln!(file, "{}, {}, save",  conf.numprocs, save_dur).unwrap();
                    writeln!(file, "{}, {}, total", conf.numprocs, frame_dur).unwrap();
                }
            }
        }
    }
    let all_dur = start_all.elapsed().as_millis() as f64 / 1000.0;

    match conf.verbose_level {
        0 | 1 => (),
        2 | _ => {
            println!();
            println!("calc duration:    {:.4}s", total_calc);
            println!("flatten duration: {:.4}s", total_flat);
            println!("save duration:    {:.4}s", total_save);
            println!("--------------------------");
            println!("total:            {:.4}s", all_dur);
        }
    };

    // ################################

    if let RenderType::Animated(vconf) = &conf.render_type {
        let command = format!("ffmpeg -y -framerate {} -i {}/image_%04d.pgm {}", vconf.fps, vconf.tmp_dir, conf.outfile);
        let _ = Command::new("sh").arg("-c").arg(command).output();
        std::fs::remove_dir_all(&vconf.tmp_dir).unwrap();
    }
}

// main calc function
fn calc(frame: Frame, width: usize, height: usize, maxiter: u32) -> Vec<Vec<u32>> {
    let ys: Vec<usize> = (0..height).collect();
    let xs: Vec<usize> = (0..width).collect();

    ys.par_iter().map(|iy| {
        let y = frame.ll.y +
            (*iy as f64 / height as f64) * (frame.ur.y - frame.ll.y);
        xs.par_iter().map(|ix| {
            let x = frame.ll.x +
                (*ix as f64 / width as f64) * (frame.ur.x - frame.ll.x);
            let mut znew;
            let mut count: u32 = 0;
            let (mut zx, mut zy) = (0.0, 0.0);

            while zx*zx+zy*zy < 256.0 && count < maxiter {
                znew = zx*zx-zy*zy + x;
                zy = 2.0*zx*zy + y;
                zx = znew;
                count += 1;
            }
            count
        }).collect()
    }).collect()
}

// ================================================

#[derive(Debug, Copy, Clone)]
struct Coord {
    x: f64,             // x component
    y: f64,             // y component
}

#[derive(Debug, Copy, Clone)]
struct Frame {
    ll: Coord,
    ur: Coord,
}


#[derive(Debug)]
#[allow(dead_code)]
struct Config {
    width: usize,       // image width in pixels
    height: usize,      // image height in pixels
    numprocs: u8,       // number of processes for omp parralelisation
    maxiter: u32,       // max number of iterations before stopping
    calc_type: u8,      // determines how the fractal is being calculated
    verbose_level: u64, // verbosity level
    outfile: String,    // image/video file to write to
    logging: Logging,   // Logging config
    render_type: RenderType, // animated vs still image
    nosave: bool,       // if true dont save the image to disk
}

#[derive(Debug)]
enum Logging {
    True(String),
    False,
}

#[derive(Debug)]
enum RenderType {
    Animated(AnimConfig),
    Still(Frame),
}

#[derive(Debug)]
struct AnimConfig {
    fps: f64,           // frames per second 
    duration: f64,      // animation duration in seconds 
    start_frame: Frame, // starting frame
    end_frame: Frame,   // end frame
    tmp_dir: String,    // path to temporarily save the pgm
}

// ################################################

impl Config {
    fn new(matches: &clap::ArgMatches) -> Config{
        let width:  usize;
        let height: usize;
        let maxiter:  u32;
        let calc_type: u8;
        let outfile: String;
        let verbose_level;
        let logging;
        let nosave: bool;
        // TODO
        let numprocs: u8;

        let render_type = if let Some(matches) = matches.subcommand_matches("animation") {
            width     = matches.value_of("width")  .unwrap() .parse().unwrap();
            height    = matches.value_of("height") .unwrap() .parse().unwrap();
            maxiter   = matches.value_of("maxiter").unwrap() .parse().unwrap();
            calc_type = matches.value_of("type")   .unwrap() .parse().unwrap();
            outfile   = matches.value_of("outfile").unwrap().to_string();
            verbose_level   = matches.occurrences_of("verbose");
            logging         = if matches.is_present("logging") {
                Logging::True(matches.value_of("logging").unwrap().to_string())
            } else {
                Logging::False
            };
            numprocs = 4;
            nosave = false;

            let fps      = matches.value_of("fps")     .unwrap().parse().unwrap();
            let duration = matches.value_of("duration").unwrap().parse().unwrap();

            let argvec_start: Vec<_> = if matches.occurrences_of("coord_start") == 1 {
                matches.values_of("coord_start").unwrap().map(|x| x.parse().unwrap()).collect()
            } else {
                vec![-1.5, 0.5, -1.0, 1.0]
            };
            let argvec_end: Vec<_> = if matches.occurrences_of("coord_end") == 1 {
                matches.values_of("coord_end").unwrap().map(|x| x.parse().unwrap()).collect()
            } else {
                vec![-1.5, -1., -0.25, 0.25]
            };
            // convert to frames
            let start_frame = Frame::from_vec(argvec_start);
            let end_frame = Frame::from_vec(argvec_end);

            // path to cache the rendered images
            let tmp_dir = "/tmp/frustal/images".to_string();
            // clear all previous files
            std::fs::remove_dir_all(tmp_dir.clone()).ok();    // ok since its not guaranteed to exist
            std::fs::create_dir_all(tmp_dir.clone()).unwrap();

            let animation_conf = AnimConfig::new(fps, duration, start_frame, end_frame, tmp_dir);
            RenderType::Animated(animation_conf)
        } else {
            width     = matches.value_of("width")  .unwrap().parse().unwrap();
            height    = matches.value_of("height") .unwrap().parse().unwrap();
            maxiter   = matches.value_of("maxiter").unwrap().parse().unwrap();
            calc_type = matches.value_of("type")   .unwrap().parse().unwrap();
            outfile   = matches.value_of("outfile").unwrap().to_string();
            verbose_level   = matches.occurrences_of("verbose");
            logging         = if matches.is_present("logging") {
                Logging::True(matches.value_of("logging").unwrap().to_string())
            } else {
                Logging::False
            };
            numprocs = matches.value_of("numprocs").unwrap().parse().unwrap();
            nosave = matches.is_present("nosave");

            // Get the still frame / set a default
            let argvec: Vec<_> = if matches.occurrences_of("coord") == 1 {
                matches.values_of("coord").unwrap().map(|x| x.parse().unwrap()).collect()
            } else {
                vec![-1.5, 0.5, -1.0, 1.0]
            };
            let frame = Frame::from_vec(argvec);
            RenderType::Still(frame)
        };

        Config{
            width, height, numprocs, maxiter,
            calc_type, verbose_level, outfile,
            render_type, logging, nosave,
        }
    } 

    fn print(&self) {
        println!("Config: ");
        println!("Res:      {}x{}", self.width, self.height);
        println!("Procs:    {}", self.numprocs);
        println!("Maxiter:  {}", self.maxiter);
        if let Logging::True(logfile) = &self.logging {
            println!("logfile:  {}", logfile);
        }
    }

    fn iter(&self) -> ConfigIter {
        match &self.render_type {
            RenderType::Animated(conf) => {
                ConfigIter {
                    start_frame: &conf.start_frame,
                    end_frame: &conf.end_frame,
                    cur: 0,
                    total: (conf.fps * conf.duration) as usize
                }
            },
            RenderType::Still(frame) => {
                ConfigIter { 
                    start_frame: &frame,
                    end_frame: &frame,
                    cur: 0,
                    total: 0
                }
            }
        }
    }
}

struct ConfigIter<'a> {
    start_frame: &'a Frame,
    end_frame: &'a Frame,
    cur: usize,
    total: usize,
}

impl<'a> Iterator for ConfigIter<'a> {
    type Item = Frame;

    fn next(&mut self) -> Option<Frame> {
        if self.cur > self.total {
            return None;
        }

        let r = if self.cur == 0 {
            *self.start_frame
        } else if self.cur == self.total {
            *self.end_frame
        } else {
            let frac = self.cur as f64 / self.total as f64;
            let cur_ll_x = self.start_frame.ll.x + frac * (self.end_frame.ll.x  - self.start_frame.ll.x);
            let cur_ll_y = self.start_frame.ll.y + frac * (self.end_frame.ll.y  - self.start_frame.ll.y);
            let cur_ur_x = self.start_frame.ur.x + frac * (self.end_frame.ur.x  - self.start_frame.ur.x);
            let cur_ur_y = self.start_frame.ur.y + frac * (self.end_frame.ur.y  - self.start_frame.ur.y);
            let cur_ll = Coord::new(cur_ll_x, cur_ll_y);
            let cur_ur = Coord::new(cur_ur_x, cur_ur_y);
            Frame::new(cur_ll, cur_ur)
        };
        self.cur += 1;
        Some(r)
    }
}

fn save_as_png(data: &[u8], conf: &Config, count: usize) {
    // todo dont add count number to non-animated images
    match &conf.render_type {
        RenderType::Still(_) => {
            image::save_buffer(
                format!("{}", conf.outfile), 
                data, conf.width as u32,
                conf.height as u32,
                image::ColorType::Gray(8)
                ).unwrap();
        },
        RenderType::Animated(vconf) => {
            image::save_buffer(
                format!("{}/image_{:04}.pgm", vconf.tmp_dir, count), 
                data, conf.width as u32,
                conf.height as u32,
                image::ColorType::Gray(8)
                ).unwrap();
        }
    }
}

impl AnimConfig {
    fn new(fps: f64, duration: f64, start_frame: Frame, end_frame: Frame, tmp_dir: String) ->  AnimConfig {
        AnimConfig{ fps, duration, start_frame, end_frame, tmp_dir }
    }
}

impl Coord {
    fn new(x: f64, y: f64) -> Coord {
        Coord { x, y }
    }
}

impl Frame {
    fn new(ll: Coord, ur: Coord) -> Frame {
        Frame { ll, ur }
    }

    fn from_vec(vec: Vec<f64>) -> Frame {
        Frame::new(Coord::new(vec[0], vec[2]), Coord::new(vec[1], vec[3]))
    }
}
